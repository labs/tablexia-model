/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.rest;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class UserRestPath {
    public static final String USER_PATH            = "/user";
    public static final String USER_CREATE_OLD      = "/create";
    public static final String USER_CREATE          = "/create_v321plus";
    public static final String USER_UUID            = "uuid";
    public static final String USER_CONFIRM         = "/confirm/{" + USER_UUID + "}";
    public static final String USER_GET             = "/get/{" + USER_UUID + "}";
    public static final String USER_DELETE          = "/delete/{" + USER_UUID + "}";
    public static final String UPLOAD_AVATAR        = "/upload_avatar/{" + USER_UUID + "}";
    public static final String DOWNLOAD_AVATAR      = "/download_avatar/{" + USER_UUID + "}";
    public static final String AVATAR_ACTUAL        = "/avatar_actual/{" + USER_UUID + "}";

    public static String getUserAvatarUploadUrl(String uuid) {
        return USER_PATH + UPLOAD_AVATAR.replace("{" + USER_UUID + "}", uuid);
    }

    public static String getUserAvatarDownloadUrl(String uuid) {
        return USER_PATH + DOWNLOAD_AVATAR.replace("{" + USER_UUID + "}", uuid);
    }

    public static String getUserAvatarActualUrl(String uuid) {
        return USER_PATH + AVATAR_ACTUAL.replace("{" + USER_UUID + "}", uuid);
    }

    public static String getCreateUserUrl() {
        return USER_PATH + USER_CREATE;
    }

    public static String getConfirmUserUrl(String uuid) {
        return USER_PATH + USER_CONFIRM.replace("{" + USER_UUID + "}", uuid);
    }

    public static String getDownloadUserUrl(String uuid) {
        return USER_PATH + USER_GET.replace("{" + USER_UUID + "}", uuid);
    }

    public static String getDeleteUserUrl(String uuid) {
        return USER_PATH + USER_DELETE.replace("{" + USER_UUID + "}", uuid);
    }
}