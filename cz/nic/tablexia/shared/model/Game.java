/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class Game {

    public static final int     DEFAULT_GAME_LOCALE                             = -1;
    public static final String  DEFAULT_APPLICATION_VERSION_NAME                = "UNKNOWN";
    public static final int     DEFAULT_APPLICATION_VERSION_CODE                = -1;
    public static final String  DEFAULT_MODEL_VERSION_NAME                      = "UNKNOWN";
    public static final int     DEFAULT_MODE_VERSION_CODE                       = -1;
    public static final int     DEFAULT_BUILD_TYPE                              = -1;
    public static final int     DEFAULT_PLATFORM                                = -1;
    public static final int     BACK_COMPATIBILITY_321_RANK_SYSTEM_VERSION_CODE = 1;
    public static final int     DEFAULT_USER_RANK                               = -1;
    public static final int     DEFAULT_USER_AGE                                = -1;

    private static final String DATE_AND_TIME_FORMAT = "d.M.yyyy HH:mm:ss", DATE_FORMAT = "d.M.yyyy";

    private long    id;
    private User    user;

    private int     gameDifficulty;
    private int     gameNumber;
    private int     gameLocale;

    private Long    randomSeed;
    private Long    startTime;
    private Long    endTime;

    private String  applicationVersionName;
    private int     applicationVersionCode;
    private String  modelVersionName;
    private int     modelVersionCode;

    private Integer rankSystemVersionCode;
    private Integer gameScoreResolverVersion;

    private int     buildType;
    private int     platform;
    private String  hwSerialNumber;

    private int userRank;
    private int userAge;

    private List<GameScore> gameScoreMap;
    private List<GamePause> gamePauses;

    // needed for JSON de/serialization
    public Game() {

    }

    public Game(Long id, User user, int difficulty, int gameNumber, Long random, Long startTime, Long endTime, int gameLocale, String applicationVersionName, Integer applicationVersionCode, String modelVersionName, Integer modelVersionCode, Integer buildType, Integer platform, String hwSerialNumber, Integer rankSystemVersionCode, Integer gameScoreResolverVersion, Integer userRank, Integer userAge) {
        this.id = id;
        this.user = user;
        this.gameDifficulty = difficulty;
        this.gameNumber = gameNumber;
        this.gameLocale = gameLocale;
        this.randomSeed = random;
        this.startTime = startTime;
        this.endTime = endTime;

        this.applicationVersionName = applicationVersionName == null ? DEFAULT_APPLICATION_VERSION_NAME : applicationVersionName;
        this.applicationVersionCode = applicationVersionCode == null ? DEFAULT_APPLICATION_VERSION_CODE : applicationVersionCode;
        this.modelVersionName       = modelVersionName == null ? DEFAULT_MODEL_VERSION_NAME : modelVersionName;
        this.modelVersionCode       = modelVersionCode == null ? DEFAULT_MODE_VERSION_CODE : modelVersionCode;
        this.buildType              = buildType == null ? DEFAULT_BUILD_TYPE : buildType;
        this.platform               = platform == null ? DEFAULT_PLATFORM : platform;

        this.rankSystemVersionCode  = rankSystemVersionCode;
        this.gameScoreResolverVersion = gameScoreResolverVersion;

        this.userRank = userRank == null ? DEFAULT_USER_RANK : userRank;
        this.userAge = userAge == null ? DEFAULT_USER_AGE : userAge;

        this.hwSerialNumber = hwSerialNumber;

        gameScoreMap = new ArrayList<GameScore>();
        gamePauses = new ArrayList<GamePause>();
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public int getGameDifficulty() {
        return gameDifficulty;
    }

    public int getGameNumber() {
        return gameNumber;
    }

    public int getGameLocale() {
        return gameLocale;
    }

    public List<GameScore> getGameScoreMap() {
        return gameScoreMap;
    }

    public List<GamePause> getGamePauses() {
        return gamePauses;
    }

    public String getApplicationVersionName() {
        return applicationVersionName;
    }

    public int getApplicationVersionCode() {
        return applicationVersionCode;
    }

    public String getModelVersionName() {
        return modelVersionName;
    }

    public int getModelVersionCode() {
        return modelVersionCode;
    }

    public int getBuildType() {
        return buildType;
    }

    public int getPlatform() {
        return platform;
    }

    public String getHwSerialNumber() {
        return hwSerialNumber;
    }

    public long getRandomSeed() {
        return randomSeed;
    }

    public void setGamePauses(List<GamePause> gamePauses) {
        this.gamePauses = gamePauses;
    }

    public void setGameScoreMap(List<GameScore> gameScoreMap) {
        this.gameScoreMap = gameScoreMap;
    }

    public Long getGameStartTime() {
        return startTime != null ? startTime : 0;
    }

    public String getGameStartDate(boolean dateAndTime) {

        if (startTime == null) return "";

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(startTime);
        SimpleDateFormat sdf;
        if (dateAndTime) {
            sdf = new SimpleDateFormat(DATE_AND_TIME_FORMAT);
        } else {
            sdf = new SimpleDateFormat(DATE_FORMAT);
        }
        return sdf.format(cal.getTime());
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public Long getGameDuration() {
        return getEndTime() - getStartTime() - getPausesDuration();
    }

    public Long getPausesDuration() {
        long pausesDuration = 0l;

        if(getGamePauses() == null) return pausesDuration;

        for(GamePause pause : getGamePauses()) {
            if(pause.hasStartTime() && pause.hasEndTime()) pausesDuration += pause.getEndTime() - pause.getStartTime();
        }
        return pausesDuration;
    }

    public void setRankSystemVersionCode(Integer rankSystemVersionCode) {
        this.rankSystemVersionCode = rankSystemVersionCode;
    }

    public Integer getRankSystemVersionCode() {
        return rankSystemVersionCode;
    }

    public Integer getGameScoreResolverVersion() {
        return gameScoreResolverVersion;
    }

    public boolean isStarted() {
        return startTime != null;
    }

    public boolean isFinished() {
        return endTime != null;
    }

    public int getUserRank() {
        return userRank;
    }

    public int getUserAge() {
        return userAge;
    }

    public GameScore getGameScore(String key) {
        for (GameScore score : getGameScoreMap()) {
            if (score.getKey().equals(key)) {
                return score;
            }
        }

        return null;
    }

    public String getGameScoreValue(String key) {
        GameScore score = getGameScore(key);
        if (score == null) {
            return null;
        }

        return score.getValue();
    }

    public String getGameScore(String key, String defaultValue) {
        GameScore gameScore = getGameScore(key);
        return (gameScore != null && gameScore.getValue() != null) ? gameScore.getValue() : defaultValue;
    }

    @Override
    public String toString() {
        String output = "GAME[id: " + id + ", user: " + (user == null ? "unknown" : user.getName()) + ", difficulty: " + gameDifficulty + ", game: " + gameNumber + ", locale: " + gameLocale + ", random: " + randomSeed + ", start: " + startTime + ", end: " + endTime + "]";
        output = output + " SCORE: [";
        for (GameScore gameScore : gameScoreMap) {
            output = output + " " + gameScore.getKey() + " : " + gameScore.getValue();
        }
        output = output + "]";
        return output;
    }

}
