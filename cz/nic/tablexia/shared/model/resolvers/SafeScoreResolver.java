/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by Aneta Steimarová on 4.5.17.
 */

public class SafeScoreResolver extends AbstractGameScoreResolver {
    public static final String SCORE_COUNT = "score_count";

    public enum ResultStars {

        THREE_STAR(GameResultDefinition.THREE_STAR, 9),
        TWO_STAR(GameResultDefinition.TWO_STAR, 6),
        ONE_STAR(GameResultDefinition.ONE_STAR, 3),
        NO_STAR(GameResultDefinition.NO_STAR, -4);

        private GameResultDefinition gameResult;
        private int score;


        ResultStars(GameResultDefinition gameResult, int score) {
            this.gameResult = gameResult;
            this.score = score;
        }

        public static GameResultDefinition getGameResultForScore(int score) {
            for (ResultStars star : ResultStars.values()) {
                if (score >= star.score) {
                    return star.gameResult;
                }
            }
            return GameResultDefinition.NO_STAR;
        }

        public static int getScoreForGameResult(GameResultDefinition result) {
            for(ResultStars star : ResultStars.values()) {
                if(star.gameResult == result) {
                    return star.score;
                }
            }
            return 0;
        }
    }


    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        int gameScore = Integer.valueOf(game.getGameScore(SCORE_COUNT, "0"));
        return ResultStars.getGameResultForScore(gameScore);
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.parseFloat(game.getGameScore(SCORE_COUNT, "0"));
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        ArrayList gameScoreList = new ArrayList<>(1);
        gameScoreList.add(new GameScore(SCORE_COUNT, String.valueOf(ResultStars.getScoreForGameResult(result))));
        return gameScoreList;
    }
}
