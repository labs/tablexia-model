/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public abstract class AbstractGameScoreResolver {
    public abstract GameResultDefinition getGameCupsResult(Game game);
    public abstract float getGameScoreResult(Game game);

    public abstract List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result);

    /**
     * Override this method if you want to use different values to sort graphs in statistics screen (eg. CrimeScene)
     */
    public float getComparisonScore(Game game) {
        return getGameScoreResult(game);
    }

    /**
     * Override this method if you want to show different formatted score
     */
    public String getFormattedScore(Game game) {
        return Integer.toString(Math.round(getGameScoreResult(game)));
    }

    /**
     * Method for formatting average score.
     * @param game is needed in inherited classes
     */
    public String getFormattedScore(Game game, float averageScore) {
        return Integer.toString(Math.round(averageScore));
    }
}