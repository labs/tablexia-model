/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class RobberyScoreResolver extends AbstractGameScoreResolver {
    public static final int         FINAL_SCORE_THREE_STARS             = 47;
    public static final int         FINAL_SCORE_TWO_STARS 	            = 29;
    public static final int         FINAL_SCORE_ONE_STAR 	            = 15;

    public static final String 		SCORE_KEY_PERSON_COUNT 				= "person_count";
    public static final String 		SCORE_KEY_THIEVES_COUNT 			= "thieves_count";
    public static final String 		SCORE_KEY_CAUGHT_THIEF 				= "caught_thief";

    public static final String 		SCORE_KEY_INNOCENCE_PERSON 			= "innocence_person";
    public static final String 		SCORE_KEY_ESCAPED_THIEVES 			= "escaped_thieves";
    public static final String      SCORE_KEY_PERSON_NUMBER 			= "person_number";

    public static final String      SCORE_KEY_GAME_RULE                 = "game_rule";

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        int creaturesCount = getFinalScore(game);

        if (creaturesCount > FINAL_SCORE_THREE_STARS) return GameResultDefinition.THREE_STAR;
        else if (creaturesCount > FINAL_SCORE_TWO_STARS) return GameResultDefinition.TWO_STAR;
        else if (creaturesCount > FINAL_SCORE_ONE_STAR) return GameResultDefinition.ONE_STAR;
        else return GameResultDefinition.NO_STAR;

    }

    @Override
    public float getGameScoreResult(Game game) {
        return getFinalScore(game);
    }

    public int getFinalScore(Game game) {
        int numOfPerson = Integer.valueOf(game.getGameScore(SCORE_KEY_PERSON_NUMBER, "0"));
        int wrongPerson = Integer.valueOf(game.getGameScore(SCORE_KEY_INNOCENCE_PERSON, "0"));
        int escapedThieves = Integer.valueOf(game.getGameScore(SCORE_KEY_ESCAPED_THIEVES, "0"));

        return numOfPerson - wrongPerson - escapedThieves;
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
            List<GameScore> gameScoreList = new ArrayList<>(3);

			switch (result) {
				case NO_STAR:
					gameScoreList.add(new GameScore(SCORE_KEY_PERSON_NUMBER, Integer.toString(FINAL_SCORE_ONE_STAR)));
					break;
				case ONE_STAR:
					gameScoreList.add(new GameScore(SCORE_KEY_PERSON_NUMBER, Integer.toString(FINAL_SCORE_ONE_STAR + 3)));
					break;
				case TWO_STAR:
					gameScoreList.add(new GameScore(SCORE_KEY_PERSON_NUMBER, Integer.toString(FINAL_SCORE_TWO_STARS + 3)));
					break;
				default:
					gameScoreList.add(new GameScore(SCORE_KEY_PERSON_NUMBER, Integer.toString(FINAL_SCORE_THREE_STARS + 3)));
					break;
			}

			//One escaped thief and one innocent person identified as a thief...
			gameScoreList.add(new GameScore(SCORE_KEY_ESCAPED_THIEVES, Integer.toString(1)));
			gameScoreList.add(new GameScore(SCORE_KEY_INNOCENCE_PERSON, Integer.toString(1)));

			return gameScoreList;
    }
}