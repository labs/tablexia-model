/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class PursuitScoreResolver extends AbstractGameScoreResolver {
    public static final int    EASY_ONE_STAR_DURATION         = 300;
    public static final int    EASY_TWO_STARS_DURATION        = 160;
    public static final int    EASY_THREE_STARS_DURATION      = 80;

    public static final int    MEDIUM_ONE_STAR_DURATION       = 600;
    public static final int    MEDIUM_TWO_STARS_DURATION      = 360;
    public static final int    MEDIUM_THREE_STARS_DURATION    = 160;

    public static final int    HARD_ONE_STAR_DURATION          = 800;
    public static final int    HARD_TWO_STARS_DURATION         = 460;
    public static final int    HARD_THREE_STARS_DURATION       = 320;

    public static final String SCORE_KEY_MOVE_COUNT            = "move_count";
    public static final String SCORE_KEY_LAST_MAP              = "last_map_index";

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        DifficultyDefinition difficulty = DifficultyDefinition.getDifficultyDefinitionForDifficultyNumber(game.getGameDifficulty());
        Long durationInMillis = game.getGameDuration();

        if (durationInMillis == null) {
            return GameResultDefinition.NO_STAR;
        }
        float durationInSeconds = durationInMillis / 1000f;
        switch (difficulty) {
            case EASY:
                if (durationInSeconds > EASY_ONE_STAR_DURATION) {
                    return GameResultDefinition.NO_STAR;
                } else if (durationInSeconds > EASY_TWO_STARS_DURATION) {
                    return GameResultDefinition.ONE_STAR;
                } else if (durationInSeconds > EASY_THREE_STARS_DURATION) {
                    return GameResultDefinition.TWO_STAR;
                } else {
                    return GameResultDefinition.THREE_STAR;
                }
            case MEDIUM:
                if (durationInSeconds > MEDIUM_ONE_STAR_DURATION) {
                    return GameResultDefinition.NO_STAR;
                } else if (durationInSeconds > MEDIUM_TWO_STARS_DURATION) {
                    return GameResultDefinition.ONE_STAR;
                } else if (durationInSeconds > MEDIUM_THREE_STARS_DURATION) {
                    return GameResultDefinition.TWO_STAR;
                } else {
                    return GameResultDefinition.THREE_STAR;
                }
            case HARD:
                if (durationInSeconds > HARD_ONE_STAR_DURATION) {
                    return GameResultDefinition.NO_STAR;
                } else if (durationInSeconds > HARD_TWO_STARS_DURATION) {
                    return GameResultDefinition.ONE_STAR;
                } else if (durationInSeconds > HARD_THREE_STARS_DURATION) {
                    return GameResultDefinition.TWO_STAR;
                } else {
                    return GameResultDefinition.THREE_STAR;
                }
        }
        throw new IllegalStateException("Unknown difficulty value");
    }

    @Override
    public float getGameScoreResult(Game game) {
        return game.getGameDuration();
    }

    @Override
    public String getFormattedScore(Game game) {
        float time = getGameScoreResult(game);

        int minutes = (int) ((time / 1000) / 60);
        int seconds = (int) ((time / 1000) % 60);

        if(minutes == 0) {
            return seconds + "s";
        } else if (seconds == 0){
            return minutes + "m ";
        } else {
            return minutes + "m " + seconds + "s";
        }
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition gameResult) {
        List<GameScore> gameScores = new ArrayList<>(1);
        gameScores.add(new GameScore(SCORE_KEY_MOVE_COUNT, Integer.toString(30)));
        return gameScores;
    }
}