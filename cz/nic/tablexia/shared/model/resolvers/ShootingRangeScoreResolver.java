/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class ShootingRangeScoreResolver extends AbstractGameScoreResolver {
    public static final int[]   CUPS_EASY        = {55, 70, 95};
    public static final int[]   CUPS_MEDIUM      = {55, 75, 105};
    public static final int[]   CUPS_HARD        = {60, 80, 110};
    public static final int[][] CUPS             = {CUPS_EASY, CUPS_MEDIUM, CUPS_HARD};

    public static final String SCORE_TOTAL       = "score_total";
    public static final String SCORE_HITS        = "score_hits";
    public static final String SCORE_MISSES      = "score_misses";
    public static final String SCORE_ERRORS      = "score_errors";
    public static final String SCORE_BAD_BOXES   = "score_bad_boxes";
    public static final String SCORE_GOOD_BOXES  = "score_good_boxes";

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        int gameScore = Integer.valueOf(game.getGameScore(SCORE_TOTAL, "0"));
        int gameDifficultyOrdinal = game.getGameDifficulty() - 1;

        if (gameScore > CUPS[gameDifficultyOrdinal][2])      return GameResultDefinition.THREE_STAR;
        else if (gameScore > CUPS[gameDifficultyOrdinal][1]) return GameResultDefinition.TWO_STAR;
        else if (gameScore > CUPS[gameDifficultyOrdinal][0]) return GameResultDefinition.ONE_STAR;
        else return GameResultDefinition.NO_STAR;
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.parseFloat(game.getGameScore(SCORE_TOTAL, "0"));
    }

    private int clamp(int var, int min, int max) {
        return (var < min) ? min : (var > max ? max : var);
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        int diffOrdinal = difficulty.number() - 1;

        diffOrdinal = clamp(diffOrdinal, 0, CUPS.length - 1);

        //if there are no CUPS for this difficulty number, use CUPS[0]
        int[] arrCups = CUPS[diffOrdinal];

        int totalScore;

        //if game result has 0 cups set total score to easy cups - 1
        //otherwise set total score to needed cups for game result + 1
        int cupsCount = result.getCupsCount();
        if(cupsCount == 0) totalScore = arrCups[0] - 1;
        else totalScore = arrCups[cupsCount - 1] + 1;

        List<GameScore> gameScores = new ArrayList<>(1);
        gameScores.add(new GameScore(SCORE_TOTAL, Integer.toString(totalScore)));
        return gameScores;
    }
}