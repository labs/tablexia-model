/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class KidnappingScoreResolver extends AbstractGameScoreResolver {
    public static final int[]   GAME_RULE_CUPS_EASY   = {0, 1, 3};
    public static final int[]   GAME_RULE_CUPS_MEDIUM = {0, 2, 4};
    public static final int[]   GAME_RULE_CUPS_HARD   = GAME_RULE_CUPS_MEDIUM;

    public static final int[][] GAME_RULE_CUPS        = {GAME_RULE_CUPS_EASY, GAME_RULE_CUPS_MEDIUM, GAME_RULE_CUPS_HARD};

    public static final String  GAME_MISSES           = "GAME_MISSES";
    public static final String  GAME_REPLAYS          = "GAME_REPLAYS";
    public static final String  GAME_STEP             = "GAME_STEP";

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        int[] levels = GAME_RULE_CUPS[game.getGameDifficulty() - 1];
        if (game.getGameScoreValue(GAME_MISSES) != null) {
            int misses = Integer.valueOf(game.getGameScoreValue(GAME_MISSES));
            if (misses <= levels[0]) {
                return GameResultDefinition.THREE_STAR;
            } else if (misses <= levels[1]) {
                return GameResultDefinition.TWO_STAR;
            } else if (misses <= levels[2]) {
                return GameResultDefinition.ONE_STAR;
            }
        }
        return GameResultDefinition.NO_STAR;
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.parseFloat(game.getGameScore(GAME_MISSES, "0"));
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        int[] levels = GAME_RULE_CUPS[difficulty.number() - 1];
        int misses;

        switch (result) {
            case NO_STAR:
                misses = levels[2] + 1;
                break;
            case ONE_STAR:
                misses = levels[2];
                break;
            case TWO_STAR:
                misses = levels[1];
                break;
            default:
                misses = 0;
                break;
        }

        List<GameScore> gameScores = new ArrayList<>(1);
        gameScores.add(new GameScore(GAME_MISSES, Integer.toString(misses)));
        return gameScores;
    }
}
