/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class RunesScoreResolver extends AbstractGameScoreResolver {
    public static final String  SCORE_TOTAL   = "score_total";
    public static final String  WRONG_RUNES   = "wrong_runes";

    public static final int[]   CUPS_EASY     = {15, 26, 38};
    public static final int[]   CUPS_MEDIUM   = {15, 26, 38};
    public static final int[]   CUPS_HARD     = {15, 26, 38};
    public static final int[][] CUPS          = {CUPS_EASY, CUPS_MEDIUM, CUPS_HARD};

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        int gameScore = Integer.valueOf(game.getGameScore(SCORE_TOTAL, "0"));
        int gameDifficultyOrdinal = game.getGameDifficulty() - 1;

        if (gameScore > CUPS[gameDifficultyOrdinal][2]) {
            return GameResultDefinition.THREE_STAR;
        } else if (gameScore > CUPS[gameDifficultyOrdinal][1]) {
            return GameResultDefinition.TWO_STAR;
        } else if (gameScore > CUPS[gameDifficultyOrdinal][0]) {
            return GameResultDefinition.ONE_STAR;
        }
        return GameResultDefinition.NO_STAR;
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.parseFloat(game.getGameScore(SCORE_TOTAL, "0"));
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        ArrayList<GameScore> gameScoresList = new ArrayList<>(1);

        int[] scoreNeeded;
        int actualScore = 0;

        if(difficulty == DifficultyDefinition.TUTORIAL) {
            scoreNeeded = new int[]{0, 0, 0};
        } else {
            scoreNeeded = CUPS[difficulty.number() - 1];
        }

        //If result is 0 cups, leave actualScore 0
        if(result.getCupsCount() > 0) {
            //Set score to one more than needed for the result
            actualScore = scoreNeeded[result.getCupsCount() - 1] + 1;
        }

        gameScoresList.add(new GameScore(SCORE_TOTAL, String.valueOf(actualScore)));
        return gameScoresList;
    }
}