/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class NightWatchScoreResolver_V3_4 extends NightWatchScoreResolver {
    public static final int    SCORE_ONE_STAR_V3_4     = 4;
    public static final int    SCORE_TWO_STARS_V3_4    = 7;
    public static final int    SCORE_THREE_STARS_V3_4  = 11;

    public static final String SCORE_ROUND_COUNT       = "round_count";

    public        final int    MAX_SCORE_V3_4          = 12;
    public        final int    MIN_SCORE_V3_4          = 2;

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        return getNumberOfStarsForResult(Integer.parseInt(game.getGameScore(SCORE_ROUND_COUNT, "0")));
    }

    public static GameResultDefinition getNumberOfStarsForResult(int points) {
        if (points >= SCORE_THREE_STARS_V3_4) {
            return GameResultDefinition.THREE_STAR;
        } else if (points >= SCORE_TWO_STARS_V3_4) {
            return GameResultDefinition.TWO_STAR;
        } else if (points >= SCORE_ONE_STAR_V3_4) {
            return GameResultDefinition.ONE_STAR;
        }
        return GameResultDefinition.NO_STAR;
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.parseFloat(game.getGameScore(SCORE_ROUND_COUNT, "0"));
    }

    /**
     * Correcting old score.
     * Max score in old version is 16.
     * New max score is 12, but possible minimum is now 2.
     *
     * @return Corrected score, if old version. Otherwise former score.
     */
    @Override
    public float getComparisonScore(Game game) {
        return (int) (getGameScoreResult(game) / MAX_SCORE * (MAX_SCORE_V3_4 - MIN_SCORE_V3_4)) + MIN_SCORE_V3_4;
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        int roundsCount;

        switch (result) {
            case NO_STAR:
                roundsCount = SCORE_ONE_STAR_V3_4 - 1;
                break;
            case ONE_STAR:
                roundsCount = SCORE_TWO_STARS_V3_4 - 1;
                break;
            case TWO_STAR:
                roundsCount = SCORE_THREE_STARS_V3_4 - 1;
                break;
            default:
                roundsCount = SCORE_THREE_STARS_V3_4;
                break;
        }

        List<GameScore> gameScores = new ArrayList<>(1);
        gameScores.add(new GameScore(SCORE_ROUND_COUNT, Integer.toString(roundsCount)));
        return gameScores;
    }
}