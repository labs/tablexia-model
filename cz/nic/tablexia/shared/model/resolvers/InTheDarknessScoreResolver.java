/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class InTheDarknessScoreResolver extends AbstractGameScoreResolver {
    public static final int ERROR_COUNT_THREE_STARS = 2;
    public static final int ERROR_COUNT_TWO_STARS = 4;
    public static final int ERROR_COUNT_ONE_STAR = 8;

    public static final String SCORE_KEY_PLANNING_DURATION = "planning_duration";
    public static final String SCORE_KEY_ERRORS_COUNT = "errors_count";

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        Integer errorsCount = Integer.valueOf(game.getGameScoreValue(SCORE_KEY_ERRORS_COUNT));

        if (errorsCount < ERROR_COUNT_THREE_STARS) return GameResultDefinition.THREE_STAR;
        else if (errorsCount < ERROR_COUNT_TWO_STARS) return GameResultDefinition.TWO_STAR;
        else if (errorsCount < ERROR_COUNT_ONE_STAR) return GameResultDefinition.ONE_STAR;
        else return GameResultDefinition.NO_STAR;
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.parseFloat(game.getGameScore(SCORE_KEY_ERRORS_COUNT, "0"));
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        int errors;

        switch (result) {
            case NO_STAR:  errors = ERROR_COUNT_ONE_STAR + 1; break;
            case ONE_STAR: errors = ERROR_COUNT_ONE_STAR - 1; break;
            case TWO_STAR: errors = ERROR_COUNT_TWO_STARS - 1; break;
            default: errors = ERROR_COUNT_THREE_STARS - 1; break;
        }

        List<GameScore> gameScores = new ArrayList<>(1);
        gameScores.add(new GameScore(SCORE_KEY_ERRORS_COUNT, Integer.toString(errors)));
        return gameScores;
    }
}