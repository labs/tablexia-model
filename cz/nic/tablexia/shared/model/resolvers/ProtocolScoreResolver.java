/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by lmarik on 3.5.17.
 */

public class ProtocolScoreResolver extends AbstractGameScoreResolver {


    public enum ResultStars{
        EASY    (DifficultyDefinition.EASY,   new int[]{5 ,8  ,11}),
        MEDIUM  (DifficultyDefinition.MEDIUM, new int[]{6 ,10 ,14}),
        HARD    (DifficultyDefinition.HARD,   new int[]{9 ,13 ,17});

        DifficultyDefinition difficulty;
        int[]           scoreResult; // 0 - min one star 1 - min two star 2 - min three star

        ResultStars(DifficultyDefinition difficulty,int [] scoreResult){
            this.difficulty = difficulty;
            this.scoreResult = scoreResult;
        }

        private GameResultDefinition getResultByScore(int score){
            if(score >= scoreResult[2]) return GameResultDefinition.THREE_STAR;
            else if(score >= scoreResult[1]) return GameResultDefinition.TWO_STAR;
            else if(score >= scoreResult[0]) return GameResultDefinition.ONE_STAR;

            return GameResultDefinition.NO_STAR;
        }

        public static GameResultDefinition getResultByDifficultyAndScore(DifficultyDefinition difficulty,int score){
            for(ResultStars stars:values()){
                if(stars.difficulty == difficulty){
                    return stars.getResultByScore(score);
                }
            }

            return GameResultDefinition.NO_STAR;
        }

        public int[] getScoreResult() {
            return scoreResult;
        }
    }

    public  static final String    SCORE_CONTROLLED                  = "objects_controlled";
    public  static final String    SCORE_COUNT                       = "score_rounds";
    public  static final String    SCORE_MISTAKE_ROUND               = "rounds_mistake";
    public  static final String    FINISHED_ROUNDS                   = "finished_rounds";

    public  static final String    ROUND_TIME_GAME_SCORE_KEY         = "%d_round_time";


    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        DifficultyDefinition difficulty = DifficultyDefinition.getDifficultyDefinitionForDifficultyNumber(game.getGameDifficulty());
        int score = Integer.valueOf(game.getGameScore(SCORE_COUNT,"0"));

        return ResultStars.getResultByDifficultyAndScore(difficulty,score);
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.valueOf(game.getGameScore(SCORE_COUNT,"0"));
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        List<GameScore> scores = new ArrayList<>(1);

        ResultStars stars = ResultStars.values()[difficulty.ordinal()-1];

        int score = 0;

        switch (result){
            case THREE_STAR:
                score = stars.scoreResult[2] + 1;
                break;
            case TWO_STAR:
                score = stars.scoreResult[1] + 1;
                break;
            case ONE_STAR:
                score = stars.scoreResult[0] + 1;
                break;
        }

        scores.add(new GameScore(SCORE_COUNT,String.valueOf(score)));

        return scores;
    }
}
