/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

public class CrimeSceneScoreResolver extends AbstractGameScoreResolver {
    public enum CrimeSceneDifficulty {

        EASY(DifficultyDefinition.EASY, 3),
        MEDIUM(DifficultyDefinition.MEDIUM, 5),
        HARD(DifficultyDefinition.HARD, 7);

        private DifficultyDefinition gameDifficulty;
        private int numberOfObjects;

        CrimeSceneDifficulty(DifficultyDefinition gameDifficulty, int numberOfObjects) {
            this.gameDifficulty = gameDifficulty;
            this.numberOfObjects = numberOfObjects;
        }

        public static CrimeSceneDifficulty getCrimeSceneDifficultyForGameDifficulty(DifficultyDefinition gameDifficulty) {
            for (CrimeSceneDifficulty crimeSceneDifficulty : CrimeSceneDifficulty.values()) {
                if (crimeSceneDifficulty.gameDifficulty == gameDifficulty) {
                    return crimeSceneDifficulty;
                }
            }
            return null;
        }

        public DifficultyDefinition getGameDifficulty() {
            return gameDifficulty;
        }

        public int getNumberOfObjects() {
            return numberOfObjects;
        }

    }

    public enum ResultStars {

        THREE_STAR(GameResultDefinition.THREE_STAR, 12, 20, 28),
        TWO_STAR(GameResultDefinition.TWO_STAR, 9, 16, 23),
        ONE_STAR(GameResultDefinition.ONE_STAR, 7, 13, 19),
        NO_STAR(GameResultDefinition.NO_STAR, -1, -1, -1);

        private GameResultDefinition gameResult;
        private int easyScore;
        private int mediumScore;
        private int hardScore;

        ResultStars(GameResultDefinition gameResult, int easyScore, int mediumScore, int hardScore) {
            this.gameResult = gameResult;
            this.easyScore = easyScore;
            this.mediumScore = mediumScore;
            this.hardScore = hardScore;
        }

        public static GameResultDefinition getStarCountForDifficultyAndErrors(DifficultyDefinition gameDifficulty, int score) {
            for (ResultStars star : ResultStars.values()) {
                if (score >= star.getScoreForDifficulty(gameDifficulty)) {
                    return star.gameResult;
                }
            }
            return GameResultDefinition.NO_STAR;
        }

        public int getScoreForDifficulty(DifficultyDefinition gameDifficulty) {
            switch (gameDifficulty) {
                case EASY:
                    return easyScore;
                case MEDIUM:
                    return mediumScore;
                case HARD:
                    return hardScore;
                default:
                    return -1;
            }
        }

        public static int getScoreForGameResultAndDifficulty(GameResultDefinition result, DifficultyDefinition gameDifficulty) {
            for(ResultStars star : ResultStars.values()) {
                if(star.gameResult == result) {
                    switch (gameDifficulty) {
                        case EASY:   return star.easyScore   + 1;
                        case MEDIUM: return star.mediumScore + 1;
                        case HARD:   return star.hardScore   + 1;
                        default: return 0;
                    }
                }
            }
            return 0;
        }
    }

    private static final String FORMATTED_TEXT_FOR_STATISTICS_FORMAT = "%d / %d";

    private static final int    MAX_ROUNDS                  = 4;

    public  static final String SCORE_KEY_COUNT             = "score_count";
    public  static final String RESULT_SCORE_COUNT          = "result_score_count";
    public  static final String REPLAY_COUNT                = "replay_count";
    public  static final String FINISHED_ROUNDS             = "finished_rounds";

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        return ResultStars.getStarCountForDifficultyAndErrors(DifficultyDefinition.getDifficultyDefinitionForDifficultyNumber(game.getGameDifficulty()),
                Integer.parseInt(game.getGameScore(SCORE_KEY_COUNT, "0")));
    }

    @Override
    public float getGameScoreResult(Game game) {
        return Float.parseFloat(game.getGameScore(SCORE_KEY_COUNT, "0"));
    }

    @Override
    public String getFormattedScore(Game game) {
        return String.format(FORMATTED_TEXT_FOR_STATISTICS_FORMAT, Math.round(getGameScoreResult(game)), getMaxScoreForDifficulty(DifficultyDefinition.getDifficultyDefinitionForDifficultyNumber(game.getGameDifficulty())));
    }

    @Override
    public String getFormattedScore(Game game, float averageScore) {
        return String.format(FORMATTED_TEXT_FOR_STATISTICS_FORMAT, Math.round(averageScore), getMaxScoreForDifficulty(DifficultyDefinition.getDifficultyDefinitionForDifficultyNumber(game.getGameDifficulty())));
    }

    private static int getMaxScoreForDifficulty(DifficultyDefinition gameDifficulty) {
        int maxScore = 0;
        for(int i = 0; i < MAX_ROUNDS; i++) {
            maxScore += CrimeSceneDifficulty.getCrimeSceneDifficultyForGameDifficulty(gameDifficulty).getNumberOfObjects() + i;
        }
        return maxScore;
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        ArrayList gameScoreList = new ArrayList<>(1);
        gameScoreList.add(new GameScore(SCORE_KEY_COUNT, String.valueOf(ResultStars.getScoreForGameResultAndDifficulty(result, difficulty))));
        return gameScoreList;
    }
}