/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.resolvers;

import java.util.ArrayList;
import java.util.List;

import cz.nic.tablexia.shared.model.Game;
import cz.nic.tablexia.shared.model.GameScore;
import cz.nic.tablexia.shared.model.definitions.DifficultyDefinition;
import cz.nic.tablexia.shared.model.definitions.GameResultDefinition;

/**
 * Created by drahomir on 11/4/16.
 */
public class NightWatchScoreResolver extends AbstractGameScoreResolver {
    public static final int SCORE_ONE_STAR          = 4;
    public static final int SCORE_TWO_STARS         = 8;
    public static final int SCORE_THREE_STARS       = 13;

    public static final int LEVELS_COUNT            = 8; //max correct windows and time
    public static final int MAX_SCORE               = 16;

    public static final String SCORE_ROUND_COUNT   = "round_count";
    public static final String SCORE_WRONG_WINDOWS = "windows_count";
    public static final String SCORE_WRONG_TIME    = "time_count";

    @Override
    public GameResultDefinition getGameCupsResult(Game game) {
        int correctWindows = LEVELS_COUNT - Integer.valueOf(game.getGameScore(SCORE_WRONG_WINDOWS, "0"));
        int correctTimeCount = LEVELS_COUNT - Integer.valueOf(game.getGameScore(SCORE_WRONG_TIME, "0"));
        return getNumberOfStarsForResult(correctWindows + correctTimeCount);
    }

    public static GameResultDefinition getNumberOfStarsForResult(int points) {
        if (points > SCORE_THREE_STARS) {
            return GameResultDefinition.THREE_STAR;
        } else if (points > SCORE_TWO_STARS) {
            return GameResultDefinition.TWO_STAR;
        } else if (points > SCORE_ONE_STAR) {
            return GameResultDefinition.ONE_STAR;
        }
        return GameResultDefinition.NO_STAR;
    }

    @Override
    public float getGameScoreResult(Game game) {
        int wrongTime = Integer.parseInt(game.getGameScore(SCORE_WRONG_TIME, Integer.toString(LEVELS_COUNT)));
        int wrongWindows = Integer.parseInt(game.getGameScore(SCORE_WRONG_WINDOWS, Integer.toString(LEVELS_COUNT)));
        return 2 * LEVELS_COUNT - wrongTime - wrongWindows;
    }

    @Override
    public float getComparisonScore(Game game) {
        return getGameScoreResult(game);
    }

    @Override
    public List<GameScore> getExampleScoreForGameResult(DifficultyDefinition difficulty, GameResultDefinition result) {
        int wrongWindows;
        int wrongTimeCount;
        switch (result) {
            case NO_STAR:
                wrongWindows = LEVELS_COUNT - 1;
                wrongTimeCount = LEVELS_COUNT - 1;
                break;
            case ONE_STAR:
                wrongWindows = ((2 * LEVELS_COUNT) - SCORE_ONE_STAR - 1) / 2;
                wrongTimeCount = wrongWindows;
                break;
            case TWO_STAR:
                wrongWindows = ((2 * LEVELS_COUNT) - SCORE_TWO_STARS - 1) / 2;
                wrongTimeCount = wrongWindows;
                break;
            default:
                wrongWindows = ((2 * LEVELS_COUNT) - SCORE_THREE_STARS - 1) / 2;
                wrongTimeCount = wrongWindows;
                break;
        }
        List<GameScore> gameScores = new ArrayList<>(2);
        gameScores.add(new GameScore(SCORE_WRONG_WINDOWS, Integer.toString(wrongWindows)));
        gameScores.add(new GameScore(SCORE_WRONG_TIME,    Integer.toString(wrongTimeCount)));
        return gameScores;
    }
}