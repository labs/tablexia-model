/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class GamePause {

    private long id;
    private Long startTime;
    private Long endTime;

    // needed for JSON de/serialization
    public GamePause() {

    }

    public GamePause(Long id, Long startTime) {
        this(id, startTime, null);
    }

    public GamePause(Long id, Long startTime, Long endTime) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long getId() {
        return id;
    }

    public Long getStartTime() {
        return startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public boolean hasStartTime() {
        return startTime != null;
    }

    public boolean hasEndTime() {
        return endTime != null;
    }

    @Override
    public String toString() {
        return "GAME PAUSE[id: " + id + ", start: " + startTime + ", end: " + endTime + "]";
    }
}
