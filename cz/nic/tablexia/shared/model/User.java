/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model;

import java.util.List;

import cz.nic.tablexia.shared.model.definitions.GenderDefinition;

/**
 *
 * @author Frantisek Simon <frantisek.simon@nic.cz>
 */
public class User {

    private Long id;

    private String signature;

    private int age;

    private String name;

    private String avatar;

    private GenderDefinition gender;

    private boolean deleted;

    private boolean help;

    private boolean intro;

    private String uuid;

    private List<Game> games;

    private List<Screen> screens;

	private List<UserDifficultySettings> difficultySettings;

    // needed for JSON de/serialization
    public User() {

    }

    public User(long id, String name, int age, GenderDefinition gender, String avatar, String signature, boolean deleted, boolean help, boolean intro) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.signature = signature;
        this.avatar = avatar;
        this.deleted = deleted;
        this.help = help;
        this.intro = intro;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public GenderDefinition getGender() {
        return gender;
    }

    public void setGender(GenderDefinition gender) {
        this.gender = gender;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isHelp() {
        return help;
    }

    public void setHelp(boolean help) {
        this.help = help;
    }

    public boolean isIntro() {
        return intro;
    }

    public void setIntro(boolean intro) {
        this.intro = intro;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public List<Screen> getScreens() { return screens; }

    public void setScreens(List<Screen> screens) { this.screens = screens; }

	public List<UserDifficultySettings> getDifficultySettings() {
		return difficultySettings;
	}

	public void setDifficultySettings(List<UserDifficultySettings> difficultySettings) {
		this.difficultySettings = difficultySettings;
	}

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof User)) {
            return false;
        }
        return ((User) obj).getId().equals(getId());
    }

    @Override
    public String toString() {
        return "USER[id: " + id + ", name: " + name + ", age: " + age + ", gen: " + gender.name() + ", del: " + deleted + ", help: " + help + ", intro: " + intro + "]";
    }
}
