/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.definitions;


/**
 * Game difficulty types
 * 
 * @author Matyáš Latner
 */
public enum DifficultyDefinition implements INumberedDefinition {
    TUTORIAL(0, 0),
    EASY    (1, 1),
    MEDIUM  (2, 2),
    HARD    (3, 3);

    private final int number;
    private final int experiencePointsMultiplier;

    DifficultyDefinition(int number, int experiencePointsMultiplier) {
        this.number = number;
        this.experiencePointsMultiplier = experiencePointsMultiplier;
    }

    @Override
    public int number() {
        return number;
    }

    public int getExperiencePointsMultiplier() {
        return experiencePointsMultiplier;
    }

    public static DifficultyDefinition getDifficultyDefinitionForDifficultyNumber(int number) {
        for (DifficultyDefinition difficultyDefinition : values()) {
            if (difficultyDefinition.number == number) {
                return difficultyDefinition;
            }
        }
        return null;
    }
}
