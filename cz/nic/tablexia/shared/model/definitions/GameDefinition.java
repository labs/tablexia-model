/*
 * Copyright (C) 2016 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model.definitions;

import java.util.HashMap;
import java.util.Map;

import cz.nic.tablexia.shared.model.resolvers.AbstractGameScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.CrimeSceneScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.InTheDarknessScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.KidnappingScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.NightWatchScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.NightWatchScoreResolver_V3_4;
import cz.nic.tablexia.shared.model.resolvers.ProtocolScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.PursuitScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.RobberyScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.RunesScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.SafeScoreResolver;
import cz.nic.tablexia.shared.model.resolvers.ShootingRangeScoreResolver;

public enum GameDefinition implements INumberedDefinition {
    ROBBERY         (1, new HashMap() {{
        put(1, new RobberyScoreResolver());
    }}),
    PURSUIT         (2, new HashMap() {{
        put(1, new PursuitScoreResolver());
    }}),
    KIDNAPPING      (3, new HashMap() {{
        put(1, new KidnappingScoreResolver());
    }}),
    NIGHT_WATCH     (4, new HashMap() {{
        put(1, new NightWatchScoreResolver());
        put(2, new NightWatchScoreResolver_V3_4());
    }}),
    SHOOTING_RANGE  (5, new HashMap() {{
        put(1, new ShootingRangeScoreResolver());
    }}),
    IN_THE_DARKNESS (6, new HashMap() {{
        put(1, new InTheDarknessScoreResolver());
    }}),
    RUNES           (7, new HashMap() {{
        put(1, new RunesScoreResolver());
    }}),
    CRIME_SCENE     (8, new HashMap() {{
        put(1, new CrimeSceneScoreResolver());
    }}),
    PROTOCOL        (9, new HashMap(){{
        put(1,new ProtocolScoreResolver());
    }}),
    SAFE            (10, new HashMap(){{
        put(1, new SafeScoreResolver());
    }});

    public  static final Integer DEFAULT_GAME_RESOLVER_VERSION = 1;

    private final int                       number;
    private       Integer                   currentGameScoreResolverVersion = 0;
    private       AbstractGameScoreResolver currentGameScoreResolver;

    private final HashMap<Integer, AbstractGameScoreResolver> gameScoreResolverMap;

    GameDefinition(int number, HashMap<Integer, AbstractGameScoreResolver> gameScoreResolverMap) {
        this.number = number;
        this.gameScoreResolverMap = gameScoreResolverMap;
        prepareCurrentGameScoreResolver();
    }

    private void prepareCurrentGameScoreResolver() {
        for(Map.Entry<Integer, AbstractGameScoreResolver> scoreResolverEntry : gameScoreResolverMap.entrySet()) {
            if(scoreResolverEntry.getKey() > currentGameScoreResolverVersion) {
                this.currentGameScoreResolverVersion = scoreResolverEntry.getKey();
                this.currentGameScoreResolver = scoreResolverEntry.getValue();
            }
        }
    }

    @Override
    public int number() {
        return number;
    }

    public AbstractGameScoreResolver getCurrentGameScoreResolver() {
        return currentGameScoreResolver;
    }

    public Integer getCurrentGameScoreResolverVersion() {
        return currentGameScoreResolverVersion;
    }

    public AbstractGameScoreResolver getGameScoreResolver(Integer key) {
        return gameScoreResolverMap.get(key);
    }

    public static GameDefinition getGameDefinitionForId(int number) {
        for (GameDefinition gameDefinition : values()) {
            if (gameDefinition.number == number) {
                return gameDefinition;
            }
        }
        return null;
    }
}
