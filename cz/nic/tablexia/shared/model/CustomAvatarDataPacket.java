/*
 * Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cz.nic.tablexia.shared.model;

/**
 * Created by drahomir on 1/11/17.
 */

public class CustomAvatarDataPacket {
    private byte[] avatarData;
    private Long   syncAt;

    public CustomAvatarDataPacket() {}

    public CustomAvatarDataPacket(byte[] avatarData, Long syncAt) {
        this.avatarData = avatarData;
        this.syncAt = syncAt;
    }

    public byte[] getAvatarData() {
        return avatarData;
    }

    public void setAvatarData(byte[] avatarData) {
        this.avatarData = avatarData;
    }

    public boolean hasAvatarData() {
        return avatarData != null;
    }

    public Long getSyncAt() {
        return syncAt;
    }

    public void setSyncAt(Long syncAt) {
        this.syncAt = syncAt;
    }

    public boolean hasSyncAt() {
        return syncAt != null;
    }
}
